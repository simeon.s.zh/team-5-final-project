# COVID-19 Safe Workspace Portal
# Project tasks :
1. Using the COVID-19 API
   * Get data for COVID-19 cases from the API https://github.com/javieraviles/covidAPI 
   * Get the number of infected people from __last week__
   * If the number of infected people from last week is :
   1. Above 10% per 1 million tested (>100 000/per 1 million), then 50% of the employees can go back to the office.
   1. Between 5% and 10% (>=50000 , <=100 000/per 1 million), then 75% of the employees can go back to the office.
   1. If the ratio is below 5% (<5000/ per 1 million), then everyone can get back to the office.
   
   __NOTE : The percentages are default values and could be changed by the Admin__

## General rules (DB relations) :
1. One country has one workspace. One workspace has one floor.

2. One project can only be allocated to one country (One country can have many projects, one project can have 1 country)

3. A person can work only in one project 

4. Each office has a different floor planning (1 office has 1 floor planning, 1 floor planning can have only 1 office)

5. Every week employees at the office should be rotated (we can do this using week numbers/ odd weeks for 1 crew, even weeks for the other crew)

6. People working on the same project should be in the same time at the office (the rotation that is mentioned above)

7. Some of the employees might be on vacation and no desk should be planned for them.

8. For first two phases, employees should keep physical distance of 1.5m and not use desks next to each other (diagonal desks are allowed)
![Desks](/README+Tasks/desks.png)

## Public Part *MUST*
* Visible without authentication
* Have a homepage
* Display location of employees and their projects + if they are on vacation
* Display planned employee locations for __next week__
* Display desks and their availability
* Have a Register functionality : username, full name, country, e-mail, password + confirm password
* Form validation on __all input fields__
* Have a Login functionality : e-mail + password
* Only registered users are in the floor planning

### Public part optional :
  * Sign in with username and password

## Private Part *MUST*
* User must manage planned vacations (enter new one or cancel/delete existing one)
* User must see his own data (when and where he is planned to work from)
* See the number of infected people for their own country from last week
* Have a logout functionality

### Private part optional :
* User receives e-mail on Friday with his plans for next week (if he will be working from home or the office)

## Admin Part *MUST*
* Manage company data like Country, Desks, Projects and Employees
* Only registered users can be assigned to projects
* Configure default percentage numbers related to the phase return to the office strategy.
* Manage floor planning.
* Gather data for infected countries
* Desk assignment for next week should be automated

### Admin part optional :
* View simple reports like : how many desks are occupied/free/unavailable


## Dev Part *MUST*

* Use React, NestJS
* Beautiful and responsive UI
* Use Bootstrap 3 or 4, or use Materialize, or don't use a framework at all
* May use own design
* Use modules to split App logic
* Create several modules and use them in routing
* Use guards for access routes
* Project should pass the default ESlint config without any errors
* Use Git __and branches__
* Document the project

### Dev part optional :
* Unit testing
* Integration testing
* Lazy loading/routing
* Originality
* Different modules in Docker Containers 
* CI/CD
* Host app on the web



